<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'website');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'wordpress');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'wordpress');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'db');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';y>?<:A_bGoWZkm)Z{MQ8~8PD%|,C2^;Vk1DnpAq|.sx!va}C>UxVWgasPBstS70');
define('SECURE_AUTH_KEY',  '0UcmiNedP]=$]zYe1:1!41$mEiI|YQ[VlOBP&GCA_)J6wl<^5n|pr>Zl4t7+|Nkk');
define('LOGGED_IN_KEY',    'HwRM{fJ%XZ@bZ-O0IrB-(0f[+/`60N% 6#9EA4?,X,<w>Hc%v(-STcEKhRBJ$F.8');
define('NONCE_KEY',        '|EXHSfG`Q6XsS/]a7kZLi3@kP^BT612Z #A6U5V5Dv8XrYLryR6nmYqLu]di;kOz');
define('AUTH_SALT',        '>WrIM hf~ @0#hB`]TV3cLPjMHY9,_zLo.M,Y.[11`hgQgT*!}z=3g?T`BOII:Wr');
define('SECURE_AUTH_SALT', '=0(y/Z)`LjGhxF7B%+%a01[3]q7)3Z.aHA8>Z|U5%1 QUPLqx.e#FZu~xEul2%Q>');
define('LOGGED_IN_SALT',   'KhT]BppM];-J952@F!ks:Y!,%w>Uwm/_r1)[3lDfs*M i;uYig/6v-ZZr/[)Z b+');
define('NONCE_SALT',       '+wzYIo[hXxrT<11*TV!H`iACo|2#@0c7|<1b;<1IOEoG#1MdK;6<`,]=:1d:[vl%');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'ttyfm83_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');