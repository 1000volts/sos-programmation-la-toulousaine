<?php
		$args = array(
				'post_type'      => 'page',
				'posts_per_page' => -1,
				'post_parent'    => $post->ID,
				'order'          => 'ASC',
				'orderby'        => 'menu_order'
				);

		$parent = new WP_Query( $args );
		$posts = $parent->get_posts(); 

		wp_reset_postdata();

		$questionsArray = array();
		$categorieArray = array();		

		$show_page_title_motor = true;

		if( !get_field('show_page_title_motor') ) {
			$show_page_title_motor = false;
		}

		?> 



		<?php foreach ($posts as $post): ?>

		<?php $isExternalLink = get_field('isExternalLink',$post->ID ); ?>
				
			<?php 
			
				if (has_children($post->ID)) {
					array_push($categorieArray, $post);
				} 
				else 
				{
					if ($isExternalLink) 
					{
						array_push($categorieArray, $post);
					} 
					else 
					{
						array_push($questionsArray,$post);
					}				
				} 
			?>

		<?php endforeach ?>

		<?php if (!empty($categorieArray)): ?>

			<div class="produits-container">
			
				<?php if($show_page_title_motor) : ?>
					<h2>CLIQUEZ SUR VOTRE MOTEUR <span>CORRESPONDANT À VOTRE INSTALLATION</span></h2>
				<?php endif; ?>
						
				<ul id="liste_produits">
					<?php foreach ($categorieArray as $categorie): ?>
						<?php include(locate_template('template-parts/item_produit.php')); ?>
					<?php endforeach ?>
				</ul>
			
			</div>

				<?php if (!empty($questionsArray)): ?>
				<div class="questions-container">
				<h2>Quelle difficulté rencontrez-vous ?</h2>
				<ul id="liste_liens">
					<?php foreach ($questionsArray as $question): ?>
						<?php include(locate_template('template-parts/item_question.php')); ?>
					<?php endforeach ?>
				</ul>
				</div>
					<?php endif ?>

				

		<?php endif ?>

		<?php if (empty($categorieArray) && !empty($questionsArray)): ?>

			<?php include(locate_template('template-parts/list_questions.php')); ?>

		<?php endif ?>

