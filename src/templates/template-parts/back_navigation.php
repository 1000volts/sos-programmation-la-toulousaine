<div class="back-navigation">

	<?php 
		wp_reset_postdata();
		$queryVar = (get_query_var('parent')); 
		
		$previousUrl = "";

		if (is_singular( 'notice' )) {
			$previousUrl = home_url() . $queryVar;
		} else {
			$previousUrl = get_permalink( $post->post_parent );
		}
	?>

	<a href="<?php echo ($previousUrl); ?>" > <span class="icon-angle-left"></span><?php if (is_singular( 'notice' )) {	echo "Retour aux questions";} elseif ($post->post_parent == 9) { echo "Retour à Moteur Radio Zigbee"; } else { echo "Retour";} ?></a>

</div>