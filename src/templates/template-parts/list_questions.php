<?php get_template_part('template-parts/back_navigation') ?>

<h2>Quelle difficulté rencontrez-vous ?</h2>


<ul id="liste_liens">
		<?php foreach ($questionsArray as $question): ?>
			<?php include(locate_template('template-parts/item_question.php')); ?>
		<?php endforeach ?>
</ul>

