<?php

global $wp;

$urlCurrent = home_url( $wp->request );
$urlCurrent = parse_url($urlCurrent);

$urlNotice = get_field('link_notice', $question->ID);
$urlNotice = esc_url(add_query_arg( 'parent', $urlCurrent[path], $urlNotice));

?>

<li><a href="<?php echo($urlNotice); ?>"><span class="icon-angle-right"></span><?php echo ($question->post_title); ?></a></li>
