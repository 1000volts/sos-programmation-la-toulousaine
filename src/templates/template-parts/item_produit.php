<?php
     // Update pour mettre un lien externe dans les vignettte
     //$isExternalLink = get_field('isExternalLink',$categorie->ID );
     
     $isExternalLink = get_field('isExternalLink',$categorie->ID );

     if($isExternalLink) {
          $externalLink = get_field('externalLink',$categorie->ID );
     }

     $image = get_field('vignette_categorie',$categorie->ID);
     $size = 'vignette'; // (thumbnail, medium, large, full or custom size)

     $image_default = '<img src="' . get_template_directory_uri() . '/img/image-default.jpg" alt="vignette eveno" class="img-responsive">';	

?>

 <li class="cat-item-container">
     <div class="cat-item">

          <a href="<?php if(!$isExternalLink) { print(get_permalink($categorie->ID)); } else { echo $externalLink; } ?>" <?php if($isExternalLink) { echo('target="_blank"'); } ?> >   
               <figure>
                     <?php 
                         if ($image) 
                         {
                              echo wp_get_attachment_image( $image, $size ); 
                         } 
                         else 
                         {
                              echo $image_default;
                         } 
                    ?>
               </figure>
               <div><span class="icon-angle-right"></span><?php echo($categorie->post_title); ?></div>
          </a>


     </div>
 </li>