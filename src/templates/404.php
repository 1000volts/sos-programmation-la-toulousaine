<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>

			<!-- article -->
			<article id="post-404">

				<h1>Cette page n'existe pas ou n'existe plus</h1>
				<h2>Nous vous prions de nous excuser pour la gêne occasionnée.</h2><br>

				<a href="<?php echo home_url();?>">Retour à l'accueil</a>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
