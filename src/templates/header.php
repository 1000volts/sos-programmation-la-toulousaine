<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-5V2LP8V');</script>
		<!-- End Google Tag Manager -->

		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;600;700&display=swap" rel="stylesheet">


		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/favicon/site.webmanifest">
		<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon.ico">
		<meta name="msapplication-TileColor" content="#2d89ef">
		<meta name="msapplication-config" content="<?php echo get_template_directory_uri(); ?>/img/favicon/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">




		<?php wp_head(); ?>

		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/assets/bundle.js"></script>

	</head>
	<body <?php body_class(); ?>>

		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5V2LP8V"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		

			<!-- header -->
			<header class="site-header clear" role="banner">

				<section>
					<div class="container-page z-index-max">
						<div id="mobil_menu_btn"><div class="menu_icon_bar"></div><div class="menu_icon_bar"></div><div class="menu_icon_bar"></div></div>
						<h1 class="logo-toulousaine"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/la-toulousaine-logo.svg" alt="logo Eveno"></a></h1>

						<div class="header-contact">
							
							<div>
								<span>Une question ?</span>
								<span>Nous vous répondrons rapidement</span>
							</div>

							<a href="https://www.la-toulousaine.com/fr/contact.php" target="_blank" rel="noreferrer noopener" class="button">Nous contacter</a>						
						</div>
					</div>
				</section>

			</header>
			<!-- /header -->

			<section id="image_head">
			<div class="container-page">

				<div class="container">
					<div id="fil-ariane">

						<?php 	
							if (is_singular( 'notice' )) {

								$queryVar = (get_query_var('parent'));

								$pagesName = explode('/', $queryVar);

								echo '<p id="breadcrumbs">';												

								foreach ($pagesName as $page ) {

									if ($page != "") {
										$pageId = getPageIdByName( $page ); ?>

										<span><a href="<?php echo(get_permalink($pageId)); ?>"> <?php echo get_the_title( $pageId ); ?> </a></span> »

										<?php
									}																	
								}
								echo '</p>';
							} 
							else 
							{							
								if ( function_exists('yoast_breadcrumb') ) 
								{
									yoast_breadcrumb('
									<p id="breadcrumbs">','</p>
									');
								}
							}
						?>
					</div>

					<h2 class="entry-title">									
					<?php if ( is_home() && get_option( 'page_for_posts' ) ) : ?>
						<?php echo get_the_title( get_option( 'page_for_posts' ) ); ?>
					<?php elseif ( is_front_page() ) : ?>
						<?php echo "Dépannage"; ?>
					<?php elseif ( wp_get_post_parent_id( $post_ID ) == 9 ) : ?>	
						<?php echo "Moteur radio Zigbee"; ?>	
					<?php else : ?>	
						<?php the_title(); ?>
					<?php endif; ?>
					</h2>
				</div>
			</div>
			</section>

			<div class="container-page">
