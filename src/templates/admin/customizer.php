<?php
function gsj_customize_register( $wp_customize ) {

	$wp_customize->add_setting( 'colorscheme', array(
		'default'           => 'light',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'gsl_sanitize_colorscheme',
	) );

	$wp_customize->add_control( 'colorscheme', array(
		'type'    => 'radio',
		'label'    => __( 'Color Scheme', 'groupesaintjean' ),
		'choices'  => array(
			'groupe'  => __( 'Groupe', 'groupesaintjean' ),
			'lycee-delasalle'   => __( 'Lycée De La Salle', 'groupesaintjean' ),
			'college-jeanpaul2' => __( 'Collège Jean-Paul II', 'groupesaintjean' ),
		),
		'section'  => 'colors',
		'priority' => 5,
	) );

}
add_action( 'customize_register', 'gsj_customize_register' );

function gsl_sanitize_colorscheme( $input ) {
	$valid = array( 'groupe', 'lycee-delasalle', 'college-jeanpaul2' );

	if ( in_array( $input, $valid, true ) ) {
		return $input;
	}

	return 'groupe';
}