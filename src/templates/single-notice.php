<?php get_header(); ?>

	<main role="main">
	<!-- section -->
	<section>

	<?php get_template_part('template-parts/back_navigation') ?>

	<article class="notice-container">


		<?php $telecommande = get_field('bloc_telecommandes_compatibles') ?>
		<?php $outils = get_field('bloc_outils_necessaires') ?>

		<?php if ($telecommande['show_tel_compatible'] || $outils['show_outils_necessaires'] ): ?>
				
		 <div class="notice-header"> <!-- /Notice Header -->

		 <?php if( $telecommande['show_tel_compatible'] ): ?>
		 	<div class="notice-telecommande">		 		
		 		<p class="big-txt"><?php echo ($telecommande['titre_bloc']); ?></p>
		 		<div class="image-telecommandes">
		 			<img src="<?php echo ($telecommande['image_telecommandes_compatibles']); ?>" alt="modeles des télécommandes compatibles">
					 <?php if($telecommande['legende_telecommandes_compatibles']) : ?>
					 	<p><?php echo ($telecommande['legende_telecommandes_compatibles']); ?></p>
					 <?php endif; ?>
		 		</div>
		 	</div>
		  <?php endif ?>


		  <?php if( $outils['show_outils_necessaires'] ): ?>
		 	<div class="notice-outils">		 		
		 		<p class="big-txt"><?php echo ($outils['titre_bloc']); ?></p>
		 		<div class="image-outils">
		 			<img src="<?php echo ($outils['image_outils_necessaires']); ?>" alt="outils nécessaire">
					 <?php if($outils['nom_outil']) : ?>
		 				<p><?php echo ($outils['nom_outil']); ?></p>
					 <?php endif; ?>
		 		</div>
		 	</div>
		 	<?php endif ?>

		 </div> <!-- /Notice Header -->

		<?php endif ?>


		<div class="notice-content"> <!-- /Notice Contenu -->

			<?php if ( have_rows( 'bloc_contenu_notice' ) ) : ?> <!-- /GROUPE -->

				<?php while ( have_rows( 'bloc_contenu_notice' ) ) : the_row(); 

					if ( have_rows( 'ajouter_etape' ) ) : ?> <!-- /REPEATER ETAPE -->

						<?php while ( have_rows( 'ajouter_etape' ) ) : the_row(); 

							$titre_etape = get_sub_field( 'titre_etape' );											
						?>
							<div class="notice-etape-row">
							<h2><?php echo ($titre_etape); ?></h2>

							<?php if ( have_rows( 'sous_bloc_contenu' ) ) : ?> <!-- /SOUS REPEATER -->

								<?php while ( have_rows( 'sous_bloc_contenu' ) ) : the_row(); 
									$image = get_sub_field( 'image_notice_low' );									
									$texte = get_sub_field( 'texte_notice_low' );		
								?>

									<div class="notice-sous-etape-row">
										<?php if( get_sub_field('image_notice_low') ): ?>
											<div class="image-container">					
												<img src="<?php echo $image; ?>" alt="outils nécessaire">
											</div>
										<?php endif; ?>

										<?php if( get_sub_field('texte_notice_low') ): ?>
											<div class="txt-container">
												<?php echo $texte; ?>
											</div>
										<?php endif; ?>	

										<?php if( get_sub_field('show_button_low') ): ?>
											<div class="btn-container">
												<button><a href="<?php echo (get_sub_field('button_low')); ?>">Cliquez ici</a></button>
											</div>											
										<?php endif; ?>

									</div>

								<?php endwhile; ?> <!-- /SOUS REPEATER -->

							<?php endif; ?> <!-- /SOUS REPEATER -->

							</div>

						<?php endwhile; ?><!-- /REPEATER ETAPE -->

					<?php endif; ?><!-- /REPEATER ETAPE -->

				<?php endwhile; ?> <!-- /GROUPE -->

			<?php endif; ?> <!-- /GROUPE -->

		</div><!-- /Notice Contenu -->



		<div class="notice-footer">

			<?php $grp_oui_non = get_field('bloc_question_oui_non'); ?> <!-- /Bloc oui/non -->

			<?php 



				
			 ?>
			
			<?php if( $grp_oui_non['afficher_bloc_question'] ): ?>

				<div class="notice-footer-oui-non-container">
	
				<p class="big-txt"><?php echo($grp_oui_non['txt_question']); ?></p>

				<div class="btn-yes-no-container">

					<?php $urlOui = "#"; ?>
					<?php 
						if ($grp_oui_non['lien_bouton_oui']) {
							$queryVar = (get_query_var('parent'));
							$urlACF = $grp_oui_non['lien_bouton_oui']['url'];
							$link_target = $grp_oui_non['lien_bouton_oui']['target'];

							// si c'est un lien externe…
							if ($link_target == "_blank") {
								$urlOui = $grp_oui_non['lien_bouton_oui']['url'];
							} else {
								$urlOui = esc_url(add_query_arg( 'parent', $queryVar, $urlACF));
							}

						} 
					?>

				<a href="<?php echo($urlOui); ?>" 
					class="yes <?php if (!$grp_oui_non['lien_bouton_oui']) { echo "pointer-events-none"; } ?>"
					<?php if($link_target == "_blank") : ?> target="<?php echo esc_attr( $link_target ); ?>" rel="noreferrer noopener" <?php endif; ?>> 
					Oui <br>
					<?php if ($grp_oui_non['texte_bouton_oui']) { ?> <span><?php echo($grp_oui_non['texte_bouton_oui']); ?></span> <?php } ?>
				</a>

					<?php $urlNon = "#"; ?>
					<?php 
						if ($grp_oui_non['lien_bouton_non']) {
						$queryVar = (get_query_var('parent'));
						$urlACF = $grp_oui_non['lien_bouton_non']['url'];
						$link_target = $grp_oui_non['lien_bouton_non']['target'];
						
						// si c'est un lien externe…
						if ($link_target == "_blank") {
							$urlNon = $grp_oui_non['lien_bouton_non']['url'];
						} else {
							$urlNon = esc_url(add_query_arg( 'parent', $queryVar, $urlACF));
						}

					} ?>

				<a href="<?php echo($urlNon); ?>" 
					class="no <?php if (!$grp_oui_non['lien_bouton_non']) { echo "pointer-events-none"; } ?>"
				 	<?php if($link_target == "_blank") : ?> target="<?php echo esc_attr( $link_target ); ?>" rel="noreferrer noopener" <?php endif; ?>> 
					Non 
					<?php if ($grp_oui_non['texte_bouton_non']) { ?> <span><?php echo($grp_oui_non['texte_bouton_non']); ?></span> <?php } ?>
				</a>
				</div>
				</div>
	
			<?php endif; ?>


			<?php $grp_termine = get_field('bloc_fin'); ?> <!-- /Bloc oui/non -->

			<?php if( $grp_termine[('afficher_bloc_termine')] ): ?>
				<div class="notice-termine-container">					
					<?php echo $grp_termine[( 'texte_bloc_termine' )]; ?>
				</div>
			<?php endif; ?>

		</div>


	</article>
	
	<?php get_template_part('template-parts/back_navigation') ?>

	</section>
	<!-- /section -->
	</main>

<?php get_footer(); ?>
