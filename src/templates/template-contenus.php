<?php
/*
Template Name: Template contenus
*/

get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>

			<?php the_content(); ?>
				
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
