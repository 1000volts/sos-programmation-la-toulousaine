	 		</div><!-- container-page -->

			<!-- footer -->
			<footer class="site-footer" role="contentinfo">

			<div class="container-page">

				<div class="bandeau-toulousaine">

					<?php //colonne contact ?>
			 		<div class="bandeau-toulousaine-col-contact">

						<?php //adresse ?>
						<div class="bandeau-toulousaine-adresse">
							<address>
								<span>FTFM La Toulousaine</span>
								<span>Route de Toulouse</span>
								<span>Cs57668 Escalquens</span>
								<span><b>31676 Labège Cedex</b></span>
							</address>
						</div>
						<?php //fin adresse ?>

						<?php //phone ?>
						<div class="bandeau-toulousaine-phone">
							<img src="<?php echo get_template_directory_uri(); ?>/img/picto-tel.svg" alt="nous joindres par téléphone" class="bandeau-toulousaine-phone-picto">
							<div class="bandeau-toulousaine-phone-content">
								<span>Tél. 05 61 75 31 00</span>
								<span>Fax. 05 61 75 31 11 </span>
							</div>
						</div>
						<?php //fin phone ?>

						<?php //social ?>
						<div class="bandeau-toulousaine-social">

							<a href="https://www.youtube.com/channel/UCbdLEwKvBBbX3RBHYbNEBFw" 
								target="_blank" rel="noreferrer noopener" class="bandeau-toulousaine-social-youtube">
								Retrouvez nos vidéos sur 
								<img src="<?php echo get_template_directory_uri(); ?>/img/picto-youtube.svg" 
								alt="Youtube" class="bandeau-toulousaine-social-icon">
							</a>

							<div class="bandeau-toulousaine-social-reseaux">Ou suivez nous sur 
								<ul class="bandeau-toulousaine-social-reseaux-list">
			 						<li>
										 <a href="https://www.facebook.com/latoulousaine31750/" target="_blank" rel="noreferrer noopener">
										 <img src="<?php echo get_template_directory_uri(); ?>/img/facebook.svg" 
											alt="Youtube" class="bandeau-toulousaine-social-reseaux-icon">
									 	</a>
									</li>
			 						<li>
										 <a href="https://www.linkedin.com/company/latoulousaine" target="_blank" rel="noreferrer noopener">
										 <img src="<?php echo get_template_directory_uri(); ?>/img/linkedin.svg" 
											alt="Youtube" class="bandeau-toulousaine-social-reseaux-icon">
									 	</a>
									</li>
			 						<li>
										 <a href="https://www.instagram.com/ftfm_la_toulousaine/" target="_blank" rel="noreferrer noopener">
										 <img src="<?php echo get_template_directory_uri(); ?>/img/instagram.svg" 
											alt="Youtube" class="bandeau-toulousaine-social-reseaux-icon">
									 	</a>
									</li>
								</ul>
							</div>

						</div>
						<?php //fin social ?>

					</div>
					<?php //fin colonne contact ?>

					<?php //colonne aller sur le site ?>
					<div class="bandeau-toulousaine-col-gotosite">
						<div class="lien-site-toulousaine">
							<a href="https://www.la-toulousaine.com/fr/?gclid=EAIaIQobChMI0ozr2eDR9AIVyYTVCh2c0QgyEAAYASAAEgLBJPD_BwE" 
								target="_blank" rel="noreferrer noopener">
								<div>
									<span>Allez sur le site La Toulousaine</span>
									<span>Découvrez l'ensemble de nos produits</span>
								</div>
							</a>
						</div>
					</div>
					<?php // fin colonne aller sur le site ?>

				</div>


				<div class="copyright">
					<div class="container-page">
						©<?php echo date("Y"); ?> - Une réalisation <a class="signature" href="http://www.millevolts.fr/" target="_blank">Mille Volts</a> - 
						<a href="<?php echo get_permalink(1496); ?>">Mentions&nbsp;légales</a> - <a href="<?php echo get_permalink(1503); ?>">Plan du site</a>
					</div><!-- .site-info -->				
				</div>

				</div>

			</footer>
			<!-- /footer -->

		<?php wp_footer(); ?>



	</body>
</html>
